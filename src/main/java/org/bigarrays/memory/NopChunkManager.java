package org.bigarrays.memory;

import org.bigarrays.arrays.BigMatrix;

public class NopChunkManager extends ChunkManager {

	@Override
	public void release(BigMatrix big){
		/*
		 * this chunk manager has nothing to release as it does not limit the chunk
		 * count.
		 */
	}

	@Override
	public boolean canCreate(int currentCount, int chunkSizeBytes){
		return true;
	}

}
