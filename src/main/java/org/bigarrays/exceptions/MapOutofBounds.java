package org.bigarrays.exceptions;

import org.bigarrays.arrays.BigMatrix;

public class MapOutofBounds extends RuntimeException {

	private BigMatrix context;

	public void setContext(BigMatrix context){
		this.context = context;
	}

	public BigMatrix getContext(){
		return context;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
