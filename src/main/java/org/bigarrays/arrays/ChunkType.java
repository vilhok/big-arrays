package org.bigarrays.arrays;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public enum ChunkType {
	BYTE(0, Byte.BYTES, (x, y) -> Byte.compare(x.byteValue(), y.byteValue())),
	SHORT(1, Short.BYTES, (x, y) -> Short.compare(x.shortValue(), y.shortValue())),
	INT(2, Integer.BYTES, (x, y) -> Integer.compare(x.intValue(), y.intValue())),
	LONG(3, Long.BYTES, (x, y) -> Long.compare(x.longValue(), y.longValue())),
	FLOAT(5, Float.BYTES, (x, y) -> Float.compare(x.floatValue(), y.floatValue())),
	DOUBLE(6, Double.BYTES, (x, y) -> Double.compare(x.doubleValue(), y.doubleValue()));

	public final byte typeMagic;
	public final int bytesPerValue;
	public final Comparator<Number> cmp;

	private ChunkType(int i, int bytes, Comparator<Number> cmp){
		typeMagic = (byte) i;
		this.bytesPerValue = bytes;
		this.cmp = cmp;
	}

	public static Optional<ChunkType> valueOf(int magicType){
		return Arrays.stream(values()).filter(typeNumber -> typeNumber.typeMagic == magicType)
				.findFirst();
	}
}
