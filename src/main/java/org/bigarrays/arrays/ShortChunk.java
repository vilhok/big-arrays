package org.bigarrays.arrays;

public class ShortChunk extends Chunk {

	private short[][] data;

	ShortChunk(int i, int j, int width, int height, Number defaultValue){
		super(i, j, width, height, defaultValue, ChunkType.SHORT);
		data = new short[width][height];
	}

	@Override
	public Number getNumber(int x, int y){
		return data[x][y];
	}

	@Override
	public void setNumber(Number n, int x, int y){
		data[x][y] = n.shortValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getArray(){
		return (T) data;
	}

	@Override
	public void init(){
		short b = this.defaultValue.shortValue();
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				data[x][y] = b;
			}
		}
	}

	@Override
	public void deserialize(byte[] arr){
		// TODO not implemented
		throw new RuntimeException("Not implemented");
	}

}
