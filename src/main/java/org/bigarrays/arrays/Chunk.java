package org.bigarrays.arrays;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Comparator;

import org.bigarrays.utils.Point;

// ---------------- CHUNK CLASS-----------------------//

/**
 * Represents a single array in the dynamically allocated grid. Has references
 * to adjacent chunks for fast access.
 *
 *
 */
public abstract class Chunk {

	/**
	 * Chunk serialization header specification.
	 *
	 */
	private static class HeaderSpecs {
		private static final int VERSION_BYTE_LEN = 1;
		private static final int MAGIC_CHUNK_TYPE_LEN = 1;
		private static final int WIDTH_BYTES_LEN = 4;
		private static final int HEIGHT_BYTES_LEN = 4;
		private static final int LOWER_LEFT_X_BYTES_LEN = 4;
		private static final int LOWER_LEFT_Y_BYTES_LEN = 4;
		private static final int DEFAULT_VALUE_BYTES_LEN = 8;

		// private static final int MAGIC_CHUNK_TYPE_INDEX = 1;

		private static int getHeaderLength(){
			return VERSION_BYTE_LEN //
					+ MAGIC_CHUNK_TYPE_LEN//
					+ WIDTH_BYTES_LEN //
					+ HEIGHT_BYTES_LEN//
					+ LOWER_LEFT_X_BYTES_LEN //
					+ LOWER_LEFT_Y_BYTES_LEN //
					+ DEFAULT_VALUE_BYTES_LEN;
		}
	}

	/*
	 * For serialization
	 */
	public static final int HEADER_LENGHT = HeaderSpecs.getHeaderLength();

	/*
	 * Default value to use when initializing the chunk
	 */
	public final Number defaultValue;

	protected Number relativeZero;

	/*
	 * Determines the actual type of the chunk contents
	 */
	protected final ChunkType type;

	/*
	 * size of this chunk.
	 */
	public final int width;
	public final int height;

	/*
	 * Neighboring chunks
	 */
	protected Chunk up;
	protected Chunk down;
	protected Chunk left;
	protected Chunk right;

	/*
	 * Assuming 0,0 is origin and positive values expand up and right on a
	 * traditional coordinates
	 */
	public final int lowerLeftX;
	public final int lowerLeftY;

	public final Point lowerLeft;

	/*
	 * The iteration when this chunk was accessed previously
	 */
	private long lastUsed;

	private final Comparator<Number> cmp;

	public Chunk(int i, int j, int width, int height, Number defaultValue, ChunkType c){
		this.type = c;
		lowerLeftX = i;
		lowerLeftY = j;
		this.lowerLeft = new Point(i, j);
		this.width = width;
		this.height = height;
		this.defaultValue = defaultValue;
		this.cmp = c.cmp;
	}

	public int getWidth(){
		return width;
	}

	public int getHeight(){
		return height;
	}

	public Chunk getUp(){
		return up;
	}

	public Chunk getDown(){
		return down;
	}

	public Chunk getLeft(){
		return left;
	}

	public Chunk getRight(){
		return right;
	}

	public void setUp(Chunk up){
		this.up = up;
	}

	public void setDown(Chunk down){
		this.down = down;
	}

	public void setLeft(Chunk left){
		this.left = left;
	}

	public void setRight(Chunk right){
		this.right = right;
	}

	/**
	 * Removes the chunks connections from adjacent chunks
	 */
	void disconnect(){
		if(left != null){
			left.right = null;
		}
		if(right != null){
			right.left = null;
		}
		if(up != null){
			up.down = null;
		}
		if(down != null){
			down.up = null;
		}
	}

	/**
	 * returns whether or not this chunk begins from these coordinates.
	 * 
	 * @param xx
	 * @param yy
	 * @return
	 */
	public boolean matches(int xx, int yy){
		return this.lowerLeftX == xx && this.lowerLeftY == yy;
	}

	/**
	 * returns whether or not a certain coordinate is within the bounds of this
	 * chunk.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean contains(int x, int y){
		return x >= lowerLeftX && x < lowerLeftX + width && y >= lowerLeftY
				&& y < lowerLeftY + height;

	}

	/**
	 * Checks if the chunk is certain chunk by its lowerleft coordinates. is not
	 * {@link equals()}!
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isChunk(int x, int y){
		return x == lowerLeftX && y == lowerLeftY;
	}

	@Override
	public String toString(){
		return "[" + lowerLeftX + "," + lowerLeftY + "]";
	}

	public long getLastUsed(){
		return lastUsed;
	}

	public void setLastUsed(long lastUsed){
		this.lastUsed = lastUsed;
	}

	public boolean isEmpty(){
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				if(cmp.compare(getNumber(x, y), defaultValue) != 0){
					return false;
				}
			}
		}

		return true;
	}

	public final ChunkType getChunkType(){
		return type;
	}

	/**
	 * Get this chunks byte representation. First 4 bytes are the edge length of
	 * the chunks. Next 8 are the x coordinate and then y coordinate. Finally
	 * all the data rows.
	 * 
	 * @return
	 */
	public byte[] getBytes(){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(version);
		baos.write(type.typeMagic); // chunkType magic number

		int[] values = {
				width, height, lowerLeftX, lowerLeftY
		};

		// write rest of the header
		for(int k : values){
			try{
				baos.write(ByteBuffer.allocate(4).putInt(k).array());
			}catch(IOException e){
				Thread.getDefaultUncaughtExceptionHandler()
						.uncaughtException(Thread.currentThread(), e);
			}
		}

		try{
			// default value as 8 bytes
			baos.write(numberAsBytes(defaultValue.longValue()));
		}catch(IOException e1){
			e1.printStackTrace();
		}

		// write all the numbers
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				try{
					baos.write(numberAsBytes(getNumber(x, y)));

				}catch(IOException e){
					Thread.getDefaultUncaughtExceptionHandler()
							.uncaughtException(Thread.currentThread(), e);
				}
			}
		}
		return baos.toByteArray();
	}

	public static Chunk fromBytes(byte[] bytes){
		int[] values = new int[4];
		byte[] array = new byte[4];
		int start = 2;
		for(int i = 0; i < 4; i++){
			System.arraycopy(bytes, start, array, 0, 4);
			values[i] = ByteBuffer.wrap(array).order(ByteOrder.BIG_ENDIAN).getInt();
			start += 4;

		}

		byte[] defaultval = new byte[8];
		System.arraycopy(bytes, start, defaultval, 0, 8);
		start += 8;

		int width = values[0];
		int height = values[1];
		int lowerleftx = values[2];
		int lowerlefty = values[3];

		ChunkType k = ChunkType.valueOf(bytes[HeaderSpecs.MAGIC_CHUNK_TYPE_LEN]).get();

		Chunk c = PrimitiveChunkFactory.getChunk(lowerleftx, lowerlefty, width, height,
				bytesAsNumber(defaultval, ChunkType.LONG), k, false);

		switch(k){
			case BYTE:
				int i = start;
				for(int x = 0; x < width; x++){
					for(int y = 0; y < height; y++){
						c.setNumber(bytes[i], x, y);
						i++;
					}
				}
				break;
			case SHORT:
			case INT:
			case LONG:
			case DOUBLE:
			case FLOAT:
				int size = k.bytesPerValue;

				byte[] b = new byte[size];

				for(int x = 0; x < width; x++){
					for(int y = 0; y < height; y++){
						System.arraycopy(bytes, start, b, 0, size);
						c.setNumber(bytesAsNumber(b, k), x, y);
						start += size;
					}
				}
		}
		return c;
	}

	private byte[] numberAsBytes(Number k){
		return switch(k){
			case Byte b -> new byte[]{
					k.byteValue()
				};
			case Short s -> ByteBuffer.allocate(2).putShort(s).array();
			case Integer i -> ByteBuffer.allocate(4).putInt(i).array();
			case Long l -> ByteBuffer.allocate(8).putLong(l).array();
			case Float f -> ByteBuffer.allocate(4).putFloat(f).array();
			case Double d -> ByteBuffer.allocate(8).putDouble(d).array();
			default -> new byte[0];
		};
	}

	private static Number bytesAsNumber(byte[] b, ChunkType c){
		ByteBuffer bb = ByteBuffer.wrap(b);
		return switch(c){
			case BYTE -> b[0];
			case SHORT -> bb.getShort();
			case INT -> bb.getInt();
			case LONG -> bb.getLong();
			case FLOAT -> bb.getFloat();
			case DOUBLE -> bb.getDouble();
		};
	}

	@Override
	public boolean equals(Object o){
		if(o == this)
			return true;
		if(o instanceof Chunk){
			Chunk c = (Chunk) o;

			if(c.height != this.height || c.width != this.width)
				return false;
			if(c.type != this.type)
				return false;
			if(!defaultValue.equals(c.defaultValue))
				return false;
			for(int x = 0; x < width; x++){
				for(int y = 0; y < height; y++){
					if(!this.getNumber(x, y).equals(c.getNumber(x, y))){
						return false;
					}
				}

			}
			return true;
		}
		return false;

	}

	public abstract Number getNumber(int x, int y);

	public abstract void setNumber(Number n, int x, int y);

	public abstract <T> T getArray();

	public abstract void init();

	/**
	 * This can be used to insert the raw binary data directly to this byte
	 * array.
	 * 
	 * The old implementation consists of handling the byte array, deserializing
	 * and autoboxing everything into Number -objects, which are then passed to
	 * setNumber() which the autounboxes them is of course very OOP and very
	 * polymorphism, but for performance reasons each subtype should do their
	 * own deserializing.
	 * 
	 * 
	 * @pre the arr length must be ChunkType.bytesPerValue * width * height
	 * @param arrs
	 */
	public abstract void deserialize(byte[] arr);

	static int version = 0;

	public final Point location(){
		return lowerLeft;

	}
}
