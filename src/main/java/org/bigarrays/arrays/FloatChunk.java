package org.bigarrays.arrays;

public class FloatChunk extends Chunk {

	private float[][] data;

	FloatChunk(int i, int j, int width, int height, Number defaultValue){
		super(i, j, width, height, defaultValue, ChunkType.FLOAT);
		data = new float[width][height];
	}

	@Override
	public Number getNumber(int x, int y){
		return data[x][y];
	}

	@Override
	public void setNumber(Number n, int x, int y){
		data[x][y] = n.floatValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getArray(){
		return (T) data;
	}

	@Override
	public void init(){
		float b = this.defaultValue.floatValue();
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				data[x][y] = b;
			}
		}
	}

	@Override
	public void deserialize(byte[] arr){
		// TODO Auto-generated method stub
		throw new RuntimeException("Not implemented");
	}
}
