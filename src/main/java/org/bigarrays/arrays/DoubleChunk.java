package org.bigarrays.arrays;

public class DoubleChunk extends Chunk {

	private double[][] data;

	DoubleChunk(int i, int j, int width, int height, Number defaultValue){
		super(i, j, width, height, defaultValue, ChunkType.DOUBLE
				);
		data = new double[width][height];
	}

	@Override
	public Number getNumber(int x, int y){
		return data[x][y];
	}

	@Override
	public void setNumber(Number n, int x, int y){
		data[x][y] = n.doubleValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getArray(){
		return (T) data;
	}

	@Override
	public void init(){
		double d = defaultValue.doubleValue();
		for(int x = 0; x < width; x++){
			for(int y = 0; y < width; y++){
				data[x][y] = d;
			}
		}
	}

	@Override
	public void deserialize(byte[] arr){
		// TODO not implemented
		throw new RuntimeException("Not implemented");
	}
}
