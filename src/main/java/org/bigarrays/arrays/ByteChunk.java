package org.bigarrays.arrays;

public class ByteChunk extends Chunk {

	private byte[][] data;

	ByteChunk(int i, int j, int width, int height, byte defaultValue){
		super(i, j, width, height, defaultValue, ChunkType.BYTE);
		data = new byte[width][height];
	}

	@Override
	public Number getNumber(int x, int y){
		return data[x][y];
	}

	@Override
	public void setNumber(Number n, int x, int y){
		data[x][y] = n.byteValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getArray(){
		return (T) data;
	}

	@Override
	public void deserialize(byte[] arr){
		// TODO not implemented
		throw new RuntimeException("Not implemented");
	}

	@Override
	public void init(){
		byte b = this.defaultValue.byteValue();
		for(int k = 0; k < width; k++){
			for(int l = 0; l < height; l++){
				data[k][l] = b;
			}
		}
	}

}
