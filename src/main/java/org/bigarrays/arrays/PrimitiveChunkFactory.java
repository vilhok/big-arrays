package org.bigarrays.arrays;

import java.util.function.BiFunction;

public class PrimitiveChunkFactory {

	private BiFunction<Integer, Integer, Chunk> newChunk;
	private int bytes;

	public PrimitiveChunkFactory(int width, int height, Number basevalue, ChunkType primitive){
		newChunk = switch(primitive){
			case BYTE -> (x, y) -> new ByteChunk(x, y, width, height, basevalue.byteValue());
			case SHORT -> (x, y) -> new ShortChunk(x, y, width, height, basevalue.shortValue());
			case INT -> (x, y) -> new IntChunk(x, y, width, height, basevalue.intValue());
			case LONG -> (x, y) -> new LongChunk(x, y, width, height, basevalue.longValue());
			case FLOAT -> (x, y) -> new FloatChunk(x, y, width, height, basevalue.floatValue());
			case DOUBLE -> (x, y) -> new DoubleChunk(x, y, width, height, basevalue.doubleValue());
		};
		bytes = width * height * primitive.bytesPerValue;
	}

	public int chunkSizeBytes(){
		return bytes;
	}

	public Chunk get(int x, int y){
		return newChunk.apply(x, y);
	}

	public Chunk preInitialized(int x, int y){
		Chunk c = get(x, y);
		c.init();
		return c;
	}

	public static Chunk getChunk(int i, int j, int width, int height, Number basevalue,
			ChunkType primitive, boolean init){
		Chunk k = new PrimitiveChunkFactory(width, height, basevalue, primitive).get(i, j);

		if(init){
			k.init();
		}
		return k;
	}

}
