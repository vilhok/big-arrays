package util;

import java.io.File;
import java.nio.file.Paths;

public class FSUtil {

	static String base = "bigarraytest";
	static long increment = 0;
	static long testStartTime = System.currentTimeMillis();
	static String tmp = System.getProperty("java.io.tmpdir");

	public static synchronized File nextTmpFile(String prefix){
		return Paths.get(tmp, prefix, base + "-" + testStartTime + "-" + String.valueOf(increment++)).toFile();
	}
}
