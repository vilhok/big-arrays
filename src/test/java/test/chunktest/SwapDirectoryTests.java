package test.chunktest;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.bigarrays.arrays.BigMatrix;
import org.junit.jupiter.api.Test;

import util.FSUtil;

public class SwapDirectoryTests {

	private BigMatrix getMatrix(File rootfile){
		return new BigMatrix.Builder()//
				.setRoot(rootfile)//
				.build();
	}

	@Test
	public void uniqueRootViolationTest(){
		File f = FSUtil.nextTmpFile("uniqueroot");
		getMatrix(f);
		assertThrows(RuntimeException.class, () -> getMatrix(f));
	}

	@Test
	public void swapWithoutRootThrowsRuntimeException(){
		BigMatrix bx = new BigMatrix.Builder().setMaxChunksLoaded(1).build();
		bx.set(0, 0, 0);
		int targetY = bx.HEIGHT;
		int targetX = bx.WIDTH;

		assertThrows(RuntimeException.class, () -> bx.set(0, targetX, targetY));
	}

	@Test
	public void tryNonEmptySwapDirectory() throws IOException{

		File f = FSUtil.nextTmpFile("nonempty");
		f.mkdirs();
		File file = FSUtil.nextTmpFile("f");
		File root = Paths.get(f.getAbsolutePath(), file.getName()).toFile();
		root.createNewFile();
		assertThrows(RuntimeException.class,
				() -> new BigMatrix.Builder().setRoot(root).setMaxChunksLoaded(1).build());
	}

}
