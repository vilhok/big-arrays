package test.chunktest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.bigarrays.arrays.Chunk;
import org.bigarrays.arrays.ChunkType;
import org.bigarrays.arrays.PrimitiveChunkFactory;
import org.junit.jupiter.api.Test;

public class PrimitiveTest {

	@Test
	public void aSetValueOnChunkCanBeRetrieved(){

		for (ChunkType c : ChunkType.values()){
			int size = 100;
			Chunk a = PrimitiveChunkFactory.getChunk(0, 0, size, size, 0, c, true);
			Integer content = 1;
			int x = 5;
			int y = 6;
			a.setNumber(content, x, y);

			assertEquals(a.getNumber(x, y).longValue(), content.longValue());

		}
	}
}
