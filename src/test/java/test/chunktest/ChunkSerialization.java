package test.chunktest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.ThreadLocalRandom;

import org.bigarrays.arrays.Chunk;
import org.bigarrays.arrays.ChunkType;
import org.bigarrays.arrays.PrimitiveChunkFactory;
import org.junit.jupiter.api.Test;

public class ChunkSerialization {

	@Test
	public void randomlyFilledSerialiazedChunkDeserializesCorrectlyforAnyChunkType(){
		for (ChunkType type : ChunkType.values()){
			Chunk a = PrimitiveChunkFactory.getChunk(0, 0, 100, 100, 0, type, true);
			ThreadLocalRandom tr = ThreadLocalRandom.current();
			for (int i = 0; i < 10000; i++){
				a.setNumber(tr.nextLong() * Math.random(), tr.nextInt(a.width), tr.nextInt(a.height));
			}

			byte[] h = a.getBytes();

			Chunk b = Chunk.fromBytes(h);

			assertEquals(a, b);
		}
	}
}
