package test.simulationtest;

//@formatter:off
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

//@formatter:on
import java.util.concurrent.ThreadLocalRandom;

import org.bigarrays.arrays.BigMatrix;
import org.junit.jupiter.api.Test;

import util.FSUtil;

public class ChunkLoadTest {

	@Test
	public void swappingWithoutSwapFileFailsOnPositiveSide(){
		int width = 10;
		int height = 10;
		BigMatrix bm = new BigMatrix.Builder().setWidth(width).setHeight(height).setMaxChunksLoaded(1).build();

		bm.set(1, 0, 0);
		assertThrows(RuntimeException.class, () -> bm.set(1, width + 1, height + 1));
	}

	@Test
	public void swappingWithoutSwapFileFailsOnNegativeSide(){
		int width = 10;
		int height = 10;
		BigMatrix bm = new BigMatrix.Builder().setWidth(width).setHeight(height).setMaxChunksLoaded(1).build();

		bm.set(1, 0, 0);
		assertThrows(RuntimeException.class, () -> bm.set(1, -width, -height));
	}

	@Test
	public void chunkUnloadsIfBoundsAreReached(){
		BigMatrix bm = new BigMatrix.Builder().setRoot(FSUtil.nextTmpFile("junittest")).setWidth(10).setHeight(10)
				.setMaxChunksLoaded(1).build();

		bm.set(1, 0, 0);
		bm.set(1, 99, 99);
		assertTrue(bm.isDiskUsed());
	}

	@Test
	public void randomizedUnloadedChunkDeserializesCorrectly(){
		BigMatrix bm = new BigMatrix.Builder().setRoot(FSUtil.nextTmpFile("junittest")).setWidth(10).setHeight(10)
				.setMaxChunksLoaded(1).build();
		int randomVal = 11223344;
		ThreadLocalRandom tr = ThreadLocalRandom.current();
		int[][] values = bm.getChunk(0, 0).getArray();
		for (int i = 0; i < 100; i++){
			values[tr.nextInt(values.length)][tr.nextInt(values[0].length)] = tr.nextInt();
		}
		values[0][0] = randomVal;
		int[][] copy = new int[values.length][values[0].length];
		for (int i = 0; i < values.length; i++){
			for (int j = 0; j < values[i].length; j++){
				copy[i][j] = values[i][j];
			}
		}
		assertFalse(bm.isDiskUsed());
		bm.set(9, 99, 99);
		assertTrue(bm.isDiskUsed());
		values = bm.getChunk(0, 0).getArray();
		assertEquals(values[0][0], randomVal);

		for (int i = 0; i < values.length; i++){
			for (int j = 0; j < values[i].length; j++){
				assertEquals(copy[i][j], values[i][j]);
			}
		}
	}

}
